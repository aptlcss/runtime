Setup
=====


```bash
mkdir $(project-dir) && cd $(project-dir)
python3 -m venv .repo/env

curl https://mirrors.tuna.tsinghua.edu.cn/git/git-repo -o .repo/env/bin/repo
echo "export REPO_URL='https://mirrors.tuna.tsinghua.edu.cn/git/git-repo'" >> .repo/env/bin/activate*
chmod +x .repo/env/bin/repo


```

